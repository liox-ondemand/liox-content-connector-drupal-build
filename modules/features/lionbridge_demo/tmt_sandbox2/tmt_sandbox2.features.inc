<?php
/**
 * @file
 * tmt_sandbox2.features.inc
 */

/**
 * Implements hook_default_tmgmt_translator().
 */
function tmt_sandbox2_default_tmgmt_translator() {
  $items = array();
  $items['sandbox2'] = entity_import('tmgmt_translator', '{
    "name" : "sandbox2",
    "label" : "Sandbox2",
    "description" : "",
    "weight" : "0",
    "plugin" : "lionbridge",
    "settings" : {
      "auto_accept" : 0,
      "endpoint" : "https:\\/\\/developer-sandbox.liondemand.com",
      "access_key_id" : "wDUYWUJuoOXxUlkNBBAk",
      "access_key" : "dTpeKKFSbeXDpTONIRsdzxMDvaDbNAAyaTWKKnYv",
      "po_number" : "123456",
      "remote_languages_mappings" : { "en" : "en", "fr-ca" : "fr-ca", "de" : "de" }
    }
  }');
  return $items;
}
