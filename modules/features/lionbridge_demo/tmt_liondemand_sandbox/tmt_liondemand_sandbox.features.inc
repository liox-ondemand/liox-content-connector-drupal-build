<?php
/**
 * @file
 * tmt_liondemand_sandbox.features.inc
 */

/**
 * Implements hook_default_tmgmt_translator().
 */
function tmt_liondemand_sandbox_default_tmgmt_translator() {
  $items = array();
  $items['liondemand_sandbox'] = entity_import('tmgmt_translator', '{
    "name" : "liondemand_sandbox",
    "label" : "Liondemand Sandbox",
    "description" : "",
    "weight" : "0",
    "plugin" : "lionbridge",
    "settings" : {
      "auto_accept" : 1,
      "endpoint" : "https:\\/\\/developer-sandbox.liondemand.com",
      "access_key_id" : "wDUYWUJuoOXxUlkNBBAk",
      "access_key" : "dTpeKKFSbeXDpTONIRsdzxMDvaDbNAAyaTWKKnYv",
      "po_number" : "123456",
      "service" : "263",
      "currency" : "USD",
      "remote_languages_mappings" : { "en" : "en-us", "fr-ca" : "fr-ca", "de" : "de-de" }
    }
  }');
  return $items;
}
