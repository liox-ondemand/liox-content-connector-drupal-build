<?php
/**
 * @file
 * menu_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function menu_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_-propos-de-google:http://www.google.com.
  $menu_links['main-menu_-propos-de-google:http://www.google.com'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://www.google.com',
    'router_path' => '',
    'link_title' => 'À propos de Google',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_-propos-de-google:http://www.google.com',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'fr-ca',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_about-google:http://www.google.com.
  $menu_links['main-menu_about-google:http://www.google.com'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://www.google.com',
    'router_path' => '',
    'link_title' => 'About Google',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_about-google:http://www.google.com',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_ber-google:http://www.google.com.
  $menu_links['main-menu_ber-google:http://www.google.com'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://www.google.com',
    'router_path' => '',
    'link_title' => 'Über Google',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_ber-google:http://www.google.com',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'de',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>.
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_maison:<front>.
  $menu_links['main-menu_maison:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'maison',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_maison:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'fr-ca',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_zuhause:<front>.
  $menu_links['main-menu_zuhause:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Zuhause',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_zuhause:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'de',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About Google');
  t('Home');
  t('Zuhause');
  t('maison');
  t('À propos de Google');
  t('Über Google');

  return $menu_links;
}
