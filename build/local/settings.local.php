<?php

/**
 * @file
 * Drupal local environment specific configuration file.
 */

/**
 * Database settings.
 */
$databases['default']['default'] = array(
  'driver'   => 'mysql',
  'database' => 'default',
  'username' => 'default',
  'password' => 'default',
  'host'     => 'localhost',
  'port'     => '',
  'prefix'   => '',
);

$conf += array(
  // Development settings.
  'cache' => FALSE,
  'block_cache' => FALSE,
  'preprocess_css' => FALSE,
  'preprocess_js' => FALSE,
  'devel_enable' => TRUE,
  'error_level' => 2,
  'theme_debug' => TRUE,
);

$base_url = 'http://lionbridge.dev';
