#!/usr/bin/env bash

drupal_root=$(dirname "$0")/../www;
path_build=$(dirname "$0")/../build;

if [[ -z "$ENVIRONMENT" ]]; then
  echo 'The environment variable $ENVIRONMENT is not set. Valid values are "local", "dev", and "prod".';
  exit 0;
fi

# Pass all arguments to drush.
while [[ $# -gt 0 ]];
do
  drush_flags="$drush_flags $1";
  echo "$drush_flags $1";
  shift
done
drush="drush $drush_flags";

# Copy settings & local settings file.
echo 'Copying settings to sites/default...';
cp -f $path_build/$ENVIRONMENT/settings.php $drupal_root/sites/default/settings.php;
chmod 644 $drupal_root/sites/default/settings.php;
echo 'Done: copy settings to sites/default.';

echo 'Copying local settings to sites/default...';
cp -f $path_build/$ENVIRONMENT/settings.local.php $drupal_root/sites/default/settings.local.php;
chmod 644 $drupal_root/sites/default/settings.local.php;
echo 'Done: copy local settings to sites/default.';

# Set permission(s).
echo 'Setting up files permission...';
if [[ ! -d "$drupal_root/sites/default/files" ]]; then
 mkdir -p $drupal_root/sites/default/files;
fi
chmod -R 777 $drupal_root/sites/default/files;

popd
