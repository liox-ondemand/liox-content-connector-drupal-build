#!/usr/bin/env bash

path_build=$(dirname "$0");

source $path_build/common.sh;

echo "Disabling all modules we do not need on any environment.";
$drush dis $(cat $path_build/mods_purge | tr '\n' ' ') -y;

if [[ -f $path_build/$ENVIRONMENT/mods_purge ]]; then
  echo "Disabling all modules we do not need on $ENVIRONMENT environment.";
  $drush dis $(cat $path_build/$ENVIRONMENT/mods_purge | tr '\n' '') -y;
fi

echo "Uninstalling modules we do not need on any environment.";
$drush pm-uninstall $(cat $path_build/mods_purge | tr '\n' ' ') -y;

if [[ -f $path_build/$ENVIRONMENT/mods_purge ]]; then
  echo "Uninstalling modules we do not need on $ENVIRONMENT environment.";
  $drush pm-uninstall $(cat $path_build/$ENVIRONMENT/mods_purge | tr '\n' '') -y;
fi

echo "Enabling modules we need on every environment.";
$drush en $(cat $path_build/mods_enable | tr '\n' ' ') -y;

if [[ -f $path_build/$ENVIRONMENT/mods_enable ]]; then
  echo "Enabling modules we need on $ENVIRONMENT environment.";
  $drush en $(cat $path_build/$ENVIRONMENT/mods_enable | tr '\n' ' ') -y;
fi

echo "Clearing caches.";
$drush cc all -y;

echo "Reverting all features.";
$drush fra -y;

echo "Running any updates.";
$drush updb -y;

echo "Clearing caches one last time.";
$drush cc all -y;
