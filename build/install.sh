#!/usr/bin/env bash

drupal_root=$(dirname "$0")/../www;
path_build=$(dirname "$0")/../build;

pushd /var/www/sites/lionbridge.dev/www/

source "$path_build/common.sh";

sqlfile=$path_build/ref_db/lionbridge.sql
gzipped_sqlfile=$sqlfile.gz

if [ -e "$gzipped_sqlfile" ]; then
  echo "...from reference database."
  drush sql-drop -y
  zcat "$gzipped_sqlfile" | drush sqlc
elif [ -e "$sqlfile" ]; then
  echo "Reference database found.";
  echo "Dropping existing database tables."
  $drush sql-drop -yv;
  echo "Importing reference database.";
  $drush sqlc < "$path_build/ref_db/lionbridge.sql";
else
  echo "...from scratch, with Drupal minimal profile.";
  echo 'Installing drupal 7...';
  drush si standard --account-pass=drupaladm1n --site-name="Lionbridge d7" -y;
fi

source $path_build/update.sh;

# Configure xDebug.
echo "Configuring xDebug."
sudo apt-get install php5-xdebug;
echo 'Copying xDebug settings to /etc/php5/mods-available/xdebug.ini';
sudo chmod 777 /etc/php5/mods-available/xdebug.ini;
yes | cp -f $path_build/local/xdebug.ini /etc/php5/mods-available/xdebug.ini;
sudo chmod 644 /etc/php5/mods-available/xdebug.ini;
sudo service apache2 restart;
echo 'xDebug has been configured.';
