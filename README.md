# Lionbridge

## Cloning the repository

This repository uses [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

- `git clone --recurse-submodules git@github.com:promet/lb7.git`

In case you didn't clone the repository recursively, initialize the submodule with `git submodule update --init --recursive`. This command downloads the module from the module's standalone repository and checks out the commit that was recorded last for the submodule within the build repository.

## Set Up in Local

1. Clone the repository.
2. Grab the database from production (or environment needed) and put it inside `build/ref_db/lionbridge.sql`.
3. `vagrant up`
4. `vagrant ssh`
5. `cd /var/www/sites/lionbridge.dev/www`
6. `ENVIRONMENT=local ../build/install.sh`

## Environment-based Module Build

The build scripts (`install.sh` and `update.sh`) rely on the environment variable `ENVIRONMENT` (current set up uses `local`, `dev`, `prod`). The variable needs to be set prior to running the script. Depending on the environment variable, the modules listed on the directory will either be uninstalled or enabled. An example is when doing `../build/update.sh` and `ENVIRONMENT` is set to `local`, it would look for modules in the following paths &mdash; `../build/local/mods_purge` for uninstalling and `../build/local/mods_enable` for enabling.

## Module feature development - Git workflow

- Navigate to the module's root directory `cd www/modules/custom/lionbridge_translation_provider`
- When you are within the module's root directory, Git treats the module as a separate repository only showing the history for the module and not for the rest of the build repo. All other Git actions (pull, push) are also confined to the module's root directory.
- Default module branch for D7 is `7.x-1.x`. All features should be based on it.
- Create a new feature branch `git checkout -b feature-ls-XX-FEATURENAME`
- Commit changes and push to the standalone module repository. Open a PR. PRs that deal with the module code should be opened in the [standalone module's repository](https://github.com/promet/lionbridge-module). Build related PR should be opened against the [build repository](https://github.com/promet/lb7).
   - Ensure you are within the module's directory before you push.
   - Ensure you have a remote set correctly for the module. We always push to GitHub repo `git@github.com:promet/lionbridge-module.git` first. Once approved by client - sync to Bitbucket and DO.
- To pull latest version of the submodule's origin branch, run `git submodule update --recursive --remote` from the root of the parent repository.
